﻿using GildedRose.App.Entities;
using GildedRose.App.Entities.Strategies.Factory;
using GildedRose.App.Entities.Strategies.Interface;
using GildedRose.App.Repository.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GildedRose.App
{
    public class GildedRose
    {
        IList<Item> Items;
        public GildedRose(IList<Item> items)
        {
            Items = items;
        }

        public void UpdateQuality()
        {
            foreach (var item in Items)
            {
                IUpdateStrategy strategy = UpdateStrategyFactory.Create(item);
                strategy.Update(item);
            }
        }
    }
}
