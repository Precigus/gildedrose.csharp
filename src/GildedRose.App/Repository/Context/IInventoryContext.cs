﻿using System.Collections.Generic;
using GildedRose.App.Entities;

namespace GildedRose.App.Repository.Context
{
    interface IInventoryContext
    {
        IList<Item> Items { get; set; }
    }
}
