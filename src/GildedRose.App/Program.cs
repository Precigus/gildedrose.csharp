﻿using GildedRose.App.Entities;
using GildedRose.App.Repository.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GildedRose.App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("OMGHAI!");

            var items = new InventoryContext().Items.ToList();

            var app = new GildedRose(items);

            for (int i = 0; i < 31; i++)
            {
                PrintInfo(i, items);
                app.UpdateQuality();
            }

        }

        public static void PrintInfo(int day, IList<Item> items)
        {
            Console.WriteLine("-------- Day " + day + " --------");
            foreach (var item in items)
            {
                Console.WriteLine($"{item.Name}. Days left to sell: {item.SellIn}. Item quality: {item.Quality}");
            }
            Console.WriteLine("");
        }
    }
}
