﻿using GildedRose.App.Entities.Strategies.Interface;

namespace GildedRose.App.Entities.Strategies
{
    public class SulfurasUpdateStrategy : IUpdateStrategy
    {
        public void Update(Item item)
        {
            // This item does not degrade in quality and never has to be sold
        }
    }
}
