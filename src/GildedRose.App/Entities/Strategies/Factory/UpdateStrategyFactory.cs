﻿using GildedRose.App.Entities.Strategies.Interface;

namespace GildedRose.App.Entities.Strategies.Factory
{
    class UpdateStrategyFactory
    {
        public static IUpdateStrategy Create(Item item)
        {
            if (item.Name.Contains("Aged Brie"))
            {
                return new AgedBrieUpdateStrategy();
            }

            if (item.Name.Contains("Sulfuras"))
            {
                return new SulfurasUpdateStrategy();
            }

            if (item.Name.Contains("Backstage passes"))
            {
                return new BackstagePassUpdateStrategy();
            }

            if (item.Name.Contains("Conjured"))
            {
                return new ConjuredItemUpdateStrategy();
            }

            return new StandardItemUpdateStrategy();
        }
    }
}
