﻿namespace GildedRose.App.Entities.Strategies.Interface
{
    interface IUpdateStrategy
    {
        void Update(Item item);
    }
}
