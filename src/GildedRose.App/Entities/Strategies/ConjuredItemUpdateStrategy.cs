﻿using GildedRose.App.Entities.Strategies.Interface;

namespace GildedRose.App.Entities.Strategies
{
    public class ConjuredItemUpdateStrategy : IUpdateStrategy

    {
        public void Update(Item item)
        {
            if (item.Quality > 0)
            {
                item.Quality -= 2;
            }

            item.SellIn -= 1;

            if (item.SellIn < 0 && item.Quality > 0)
            {
                item.Quality -= 2;
            }
        }
    }
}
