﻿using GildedRose.App.Entities.Strategies.Interface;

namespace GildedRose.App.Entities.Strategies
{
    public class BackstagePassUpdateStrategy : IUpdateStrategy
    {
        public void Update(Item item)
        {
            if (item.Quality < 50)
            {
                item.Quality++;
            }

            if (item.SellIn < 11 && item.Quality < 50)
            {
                item.Quality++;
            }

            if (item.SellIn < 6 && item.Quality < 50)
            {
                item.Quality++;
            }

            item.SellIn -= 1;

            if (item.SellIn < 0)
            {
                item.Quality = 0;
            }
        }
    }
}
