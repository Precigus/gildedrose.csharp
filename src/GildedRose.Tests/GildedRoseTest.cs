﻿using GildedRose.App;
using System.Collections.Generic;
using GildedRose.App.Entities;
using GildedRose.App.Entities.Strategies;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GildedRose.Tests
{
    [TestClass]
    public class GildedRoseTest
    {
        [TestMethod]
        public void Foo()
        {
            Item item = new Item { Name = "foo", SellIn = 0, Quality = 0 };
            StandardItemUpdateStrategy strategy = new StandardItemUpdateStrategy();
            strategy.Update(item);
            Assert.AreNotEqual("fixme", item.Name);
        }

        [TestMethod]
        public void TestStandardUpdateQuality()
        {
            Item item = new Item { Name = "Staff of Ragnaros", SellIn = -1, Quality = 20 };
            StandardItemUpdateStrategy strategy = new StandardItemUpdateStrategy();
            strategy.Update(item);
            Assert.AreEqual(18, item.Quality);
        }

        [TestMethod]
        public void TestSulfurasUpdateQuality()
        {
            Item item = new Item { Name = "Helmet of Sulfuras", SellIn = 6, Quality = 80 };
            SulfurasUpdateStrategy strategy = new SulfurasUpdateStrategy();
            strategy.Update(item);
            Assert.AreEqual(80, item.Quality);
        }

        [TestMethod]
        public void TestAgedBrieUpdateQuality()
        {
            Item item = new Item { Name = "Aged Brie from Tamalir", SellIn = 4, Quality = 15 };
            AgedBrieUpdateStrategy strategy = new AgedBrieUpdateStrategy();
            strategy.Update(item);
            Assert.AreEqual(16, item.Quality);
        }

        [TestMethod]
        public void TestBackstagePassUpdateQuality()
        {
            Item item = new Item { Name = "Backstage passes to the Gr3ml1ns concert", SellIn = 12, Quality = 18 };
            BackstagePassUpdateStrategy strategy = new BackstagePassUpdateStrategy();
            strategy.Update(item);
            Assert.AreEqual(19, item.Quality);
        }

        [TestMethod]
        public void TestBackstagePassLessThanTenDays()
        {
            Item item = new Item { Name = "Backstage passes to the Gr3ml1ns concert", SellIn = 10, Quality = 18 };
            BackstagePassUpdateStrategy strategy = new BackstagePassUpdateStrategy();
            strategy.Update(item);
            Assert.AreEqual(20, item.Quality);
        }

        [TestMethod]
        public void TestBackstagePassLessThanFiveDays()
        {
            Item item = new Item { Name = "Backstage passes to the Gr3ml1ns concert", SellIn = 4, Quality = 22 };
            BackstagePassUpdateStrategy strategy = new BackstagePassUpdateStrategy();
            strategy.Update(item);
            Assert.AreEqual(25, item.Quality);
        }

        [TestMethod]
        public void TestBackstagePassAfterConcert()
        {
            Item item = new Item { Name = "Backstage passes to the Gr3ml1ns concert", SellIn = 0, Quality = 18 };
            BackstagePassUpdateStrategy strategy = new BackstagePassUpdateStrategy();
            strategy.Update(item);
            Assert.AreEqual(0, item.Quality);
        }

        [TestMethod]
        public void TestConjuredUpdateQuality()
        {
            Item item = new Item { Name = "Conjured Rabbit Hide", SellIn = -1, Quality = 18 };
            ConjuredItemUpdateStrategy strategy = new ConjuredItemUpdateStrategy();
            strategy.Update(item);
            Assert.AreEqual(14, item.Quality);
        }
    }
}
